// randomXYStream.cpp : Defines the entry point for the console application.
//



#include <stdio.h>
#include <cstdlib> 
#include "stdafx.h"
#include <fstream>
#include <iostream>
#include <math.h>
using namespace std;


int main()
{	int xRand; 
	int yRand;
	const int moduloVal = 100000000;
	ofstream myfile;
	int counter = 0;
	myfile.open("randomXY.csv", std::ios_base::app);
	while (1 < 2) {
		counter++;
		if (counter % moduloVal  == 0)
		{
			cout << counter <<endl;
			xRand = rand() % 100;
			yRand = rand() % 100;
			myfile << xRand << "," << yRand << endl;
		}
	}
	myfile.close();
	return 0;
}

