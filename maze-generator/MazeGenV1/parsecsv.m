function x = parsecsv(s)
%Convert a string s containing comma separated values into a matrix of
%numeric values
%
%Mike Thiem, Aug 29 2017
%mike.j.thiem@gmail.com

s = strsplit(s,char(10));

numRows = length(s);
numCols = length(strfind(s{1},',')) + 1;

x = zeros(numRows,numCols);

incompleteLines = false(numRows,1);

for iLine = 1:length(s)
    thisLine = strsplit(s{iLine},',','CollapseDelimiters',false);
    if length(thisLine) ~= numCols;
        incompleteLines(iLine) = true;
        break;
    end
    
    x(iLine,:) = str2double(thisLine);
end

if any(incompleteLines)
	x(incompleteLines,:) = NaN;
end