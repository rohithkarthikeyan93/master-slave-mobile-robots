// MazeGenV1.cpp : Defines the entry point for the console application.
// 10.29.2017 - Rohith Karthikeyan
// Maze Generator to test mapBuild feature

#include <stdio.h>
#include <cstdlib> 
#include "stdafx.h"
#include <fstream>
#include <iostream>
#include <math.h>

using namespace std;
ofstream myfile;

void startSegment()
{
	for (int i = 1; i < 8; i++)
	{
		myfile << 1 << "," << i << "," << 1 << "," << 10 - i << "," << 1 << "," << 1 << endl;
	}
	myfile << 1 << "," << 8 << "," << 1 << "," << 2 << "," << 1 << "," << 1 << endl;
	myfile << 1 << "," << 9 << "," << 1 << "," << 1 << "," << 3 << "," << 1 << endl;

}
void segment2(int n)
{
	for (int i = 1; i < 4; i++)
	{
		myfile << i + n << "," << 9 << "," << 1 << "," << 4 - i << "," << 9 << "," << 2 << endl;
	}
	
}
void segment3(int n)
{
	myfile << 1 + n << "," << 9 << "," << 1 << "," << 9 << "," << 3 << "," << 3 << endl;
	for (int i = 2; i < 9; i++)
	{
		myfile << 1+n << "," << 10-i << "," << 1 << "," << 10 - i << "," << 1 << "," << 3 << endl;
	}
	myfile << 1+n << "," << 1 << "," << 3 << "," << 1 << "," << 1 << "," << 3 << endl;

}
void segment4(int n)
{
	for (int i = 1; i < 4; i++)
	{
		myfile << i + n << "," << 1 << "," << 9 << "," << 4 - i << "," << 1 << "," << 2 << endl;
	}

}
void segment5(int n)
{
	myfile << 1 + n << "," << 1 << "," << 3 << "," << 9 << "," << 1 << "," << 1 << endl;
	for (int i = 2; i < 9; i++)
	{
		myfile << 1 + n << "," << i << "," << 1 << "," << 10 - i << "," << 1 << "," << 1 << endl;
	}
	myfile << 1 + n << "," << 9 << "," << 1 << "," << 1 << "," << 3 << "," << 1 << endl;

}

int main()
{
	int whileCounter = 0;
	int ifCounter = 0;
	const int moduloVal = 10000000;

	myfile.open("randMaze.csv", std::ios_base::app);
	startSegment();
	while(1<2) {
		if (whileCounter % moduloVal == 0) {
			segment2(ifCounter);
			ifCounter = ifCounter + 2;
			segment3(ifCounter);
			segment4(ifCounter);
			ifCounter = ifCounter + 2;
			segment5(ifCounter);
		}
		whileCounter++;

	}
	return 0;
}

