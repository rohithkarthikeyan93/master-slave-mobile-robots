%Rohith Karthikeyan - 12/1/2017
%Attempting to plot w/US and without.

%-------------------------------------------------------
% Add a code description here
%-------------------------------------------------------
clear all;
close all;
clc;

colSens = 0;
botOrOld = 1;
bPX0 = 0;
bPY0 = 0;
US10 = 0;
US20 = 0;
runCount = 0;  

%% Plot Object Graphic Attributes

masterPos = animatedline;                                       % Initialize masterPos graphics object
masterPosUS = animatedline;                                     % Initialize masterPos graphics object
mapContour = animatedline;                                      % Initialize mapContour graphics object
mapContourUS = animatedline;
setGraphicProperties(masterPos,mapContour,mapContourUS,masterPosUS);        % function initializes object graphic properties

%% File to Read
filename = 'randMaze.csv';                      % File to read data from

%% CSV Segment for Start of Maze
% rangeX/Y value determines the points selected from CSV file
rangeX = 1;                                    % Reads every new line - the values refer to the bounds of CSV cells
rangeY = 1;
increment = 1;                                  % Increment for points to be added
a = tic;                                        % Control plot DAQ rate - initialize timer
    
%% Main Loop - All graphics happen in here

while(1<2)
  
rXstr = num2str(rangeX);
rYstr = num2str(rangeY); 
rangeStr = strcat('A',rXstr,'..','F',rYstr);
b = toc(a);
if b>(1)
data = dlmread(filename,',',rangeStr);
a =tic;

[bPX,bPY,set1X,set1Y,set2X, set2Y,set3X,set3Y,bPXU,bPYU,...
    set1XU,set1YU,set2XU, set2YU, set3XU,set3YU,bPX0_out,...
    bPY0_out,botOrOld,US10,US20,runCount] = points2Draw...
    (data,botOrOld,bPX0,bPY0,US10,US20,runCount,colSens);

% clearpoints(masterPos);

addpoints(masterPos,bPX,bPY);
addpoints(masterPosUS,bPXU,bPYU);

addpoints(mapContour,set1X,set1Y);
addpoints(mapContourUS,set1XU,set1YU);

addpoints(mapContour,set2X,set2Y);
addpoints(mapContourUS,set2XU,set2YU);

addpoints(mapContour,set3X,set3Y);
addpoints(mapContourUS,set3XU,set3YU);

drawnow;

rangeX = rangeX+increment;
rangeY = rangeY+increment;

end
end

%% Function Sets Plot Object Graphic Properties

function[x, y] = setGraphicProperties(x,y,z,z1)

x.LineStyle = 'none';         
y.LineStyle = 'none';    
z.LineStyle = 'none';
z1.LineStyle = 'none';

x.Color = 'red';
y.Color = 'blue';
y.Color = 'yellow';
z1.Color = 'green';

x.Marker ='*';
y.Marker ='.';
z.Marker ='.';
z1.Marker ='*';

axis([0,20,0,10]);                              
end

%% Plot Conditional Select
function [bPX,bPY,set1X,set1Y,set2X, set2Y,set3X,set3Y,bPXU,bPYU,...
    set1XU,set1YU,set2XU, set2YU, set3XU,set3YU,bPX0_out,...
    bPY0_out,botOrOldOP,US10_out,US20_out,runCountOP] = points2Draw(...
    data,botOrOldIP,bPX0_in,bPY0_in,US10_in,US20_in,runCountIP,colSens)

botOrient = data(:,6);
bPX = data(:,1);
bPY = data(:,2);
US1 = data(:,3);
US2 = data(:,4);
US3 = data(:,5);

if (runCountIP ==0)
US10_in = US1;
US20_in = US2;
end

runCountOP = runCountIP+1;

switch botOrient
    case 1
        
        % Plots without US correction
        set1X = bPX - US1;
        set1Y = bPY;
        set2X = bPX;
        set2Y = bPY +US2;
        set3X = bPX+US3;
        set3Y = bPY;        
        
        %Plots with US correction
        bPXU = bPX0_in-(US10_in-US1);
        bPYU = bPY0_in+(US20_in-US2);
        set1XU = bPXU - US1;
        set1YU = bPYU;
        set2XU = bPXU;
        set2YU = bPYU +US2;
        set3XU = bPXU+US3;
        set3YU = bPYU; 
        
            if (colSens ==1 || ~isequal(botOrOldIP,botOrient))
               if(~isequal(botOrOldIP,botOrient))
                    bPX0_out = bPXU;
                    bPY0_out = bPYU;
                    US10_out = US1;
                    US20_out = US2;
               end
            else
                    bPX0_out = bPX0_in;
                    bPY0_out = bPY0_in;
                    US10_out = US10_in;
                    US20_out = US20_in;
            end        
    case 2
        
         % Plots without US correction
        set1X = bPX;
        set1Y = bPY+US1;
        set2X = bPX+US2;
        set2Y = bPY;
        set3X = bPX;
        set3Y = bPY-US3;  
        
         %Plots with US correction
        bPXU = bPX0_in+(US20_in-US2);
        bPYU = bPY0_in+(US10_in-US1);
        set1XU = bPXU - US1;
        set1YU = bPYU;
        set2XU = bPXU;
        set2YU = bPYU +US2;
        set3XU = bPXU+US3;
        set3YU = bPYU; 
        
            if (colSens ==1 || ~isequal(botOrOldIP,botOrient))
               if(~isequal(botOrOldIP,botOrient))
                    bPX0_out = bPXU;
                    bPY0_out = bPYU;
                    US10_out = US1;
                    US20_out = US2;
                end
            else
                    bPX0_out = bPX0_in;
                    bPY0_out = bPY0_in;
                    US10_out = US10_in;
                    US20_out = US20_in;
            end        
             
        
    case 3
          
        % Plots without US correction
        set1X = bPX +US1;
        set1Y = bPY;
        set2X = bPX;
        set2Y = bPY-US2;
        set3X = bPX - US3;
        set3Y = bPY;    
        
         %Plots with US correction
        bPXU = bPX0_in+(US10_in-US1);
        bPYU = bPY0_in-(US20_in-US2);
        set1XU = bPXU - US1;
        set1YU = bPYU;
        set2XU = bPXU;
        set2YU = bPYU +US2;
        set3XU = bPXU+US3;
        set3YU = bPYU; 
        
            if (colSens ==1 || ~isequal(botOrOldIP,botOrient))
               if(~isequal(botOrOldIP,botOrient))
                    bPX0_out = bPXU;
                    bPY0_out = bPYU;
                    US10_out = US1;
                    US20_out = US2;
               end
            else
                    bPX0_out = bPX0_in;
                    bPY0_out = bPY0_in;
                    US10_out = US10_in;
                    US20_out = US20_in;
            end        
        
    case 4
         
        % Plots without US correction
        set1X = bPX;
        set1Y = bPY-US1;
        set2X = bPX -US2;
        set2Y = bPY;
        set3X = bPX;
        set3Y = bPY+US3; 
        
          %Plots with US correction
        bPXU = bPX0_in-(US20_in-US2);
        bPYU = bPY0_in-(US10_in-US1);
        set1XU = bPXU - US1;
        set1YU = bPYU;
        set2XU = bPXU;
        set2YU = bPYU +US2;
        set3XU = bPXU+US3;
        set3YU = bPYU; 
        
            if (colSens ==1 || ~isequal(botOrOldIP,botOrient))
               if(~isequal(botOrOldIP,botOrient))
                    bPX0_out = bPXU;
                    bPY0_out = bPYU;
                    US10_out = US1;
                    US20_out = US2;
               end
            else
                    bPX0_out = bPX0_in;
                    bPY0_out = bPY0_in;
                    US10_out = US10_in;
                    US20_out = US20_in;
            end        
end
botOrOldOP = botOrient;
end

