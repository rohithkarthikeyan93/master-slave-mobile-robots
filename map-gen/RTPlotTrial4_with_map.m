%Rohith Karthikeyan - 10/30/2017
%Attempting to get both plots on same figure

%-------------------------------------------------------
% Add a code description here
%-------------------------------------------------------
clear all;
close all;
clc;
%% Plot Object Graphic Attributes
masterPos = animatedline;                       % Initialize masterPos graphics object
mapContour = animatedline;                      % Initialize mapContour graphics object
setGraphicProperties(masterPos,mapContour);     % function initializes object graphic properties
xlabel('Position - X (cm)');
ylabel('Position - Y (cm)');
title('Real-time plot of bot and map');
box on;
grid on;
pause(10);
%% File to Read
filename = 'randMaze.csv';                      % File to read data from

%% CSV Segment for Start of Maze
% rangeX/Y value determines the points selected from CSV file
rangeX = 1;                                    % Reads every new line - the values refer to the bounds of CSV cells
rangeY = 1;
increment = 1;                                  % Increment for points to be added
a = tic;                                        % Control plot DAQ rate - initialize timer


    
%% Main Loop - All graphics happen in here

while(1<2)
rXstr = num2str(rangeX);
rYstr = num2str(rangeY); 
rangeStr = strcat('A',rXstr,'..','F',rYstr);
b = toc(a);
if b>(1)
data = dlmread(filename,',',rangeStr);
a =tic;

[bPX,bPY,set1X,set1Y,set2X, set2Y,set3X,set3Y] = points2Draw(data);
%clearpoints(masterPos);
addpoints(masterPos,bPX,bPY);
addpoints(mapContour,set1X,set1Y);
addpoints(mapContour,set2X,set2Y);
addpoints(mapContour,set3X,set3Y);
drawnow;
rangeX = rangeX+increment;
rangeY = rangeY+increment;
end
end

%% Function Sets Plot Object Graphic Properties

function[x, y] = setGraphicProperties(x,y)
x.LineStyle = 'none';         
y.LineStyle = 'none';         
x.Color = 'red';
y.Color = 'blue';
x.Marker ='*';
y.Marker ='.';
axis([0,20,0,10]);                              
end

%% Plot Conditional Select
function [bPX,bPY,set1X,set1Y,set2X, set2Y,set3X,set3Y] = points2Draw(data)
botOrient = data(:,6);
bPX = data(:,1);
bPY = data(:,2);
US1 = data(:,3);
US2 = data(:,4);
US3 = data(:,5);
switch botOrient
    case 1
        set1X = bPX - US1;
        set1Y = bPY;
        set2X = bPX;
        set2Y = bPY +US2;
        set3X = bPX+US3;
        set3Y = bPY;        
    case 2
        set1X = bPX;
        set1Y = bPY+US1;
        set2X = bPX+US2;
        set2Y = bPY;
        set3X = bPX;
        set3Y = bPY-US3;  
        
    case 3
        set1X = bPX +US1;
        set1Y = bPY;
        set2X = bPX;
        set2Y = bPY-US2;
        set3X = bPX - US3;
        set3Y = bPY;         
    case 4
        set1X = bPX;
        set1Y = bPY-US1;
        set2X = bPX -US2;
        set2Y = bPY;
        set3X = bPX;
        set3Y = bPY+US3; 

end

end

