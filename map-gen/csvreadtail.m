function x = csvreadtail(filename,numLines)
%Read the last few lines of a CSV file without having to load and parse the
%entire file. It's expected that the CSV file contains exclusively numeric
%data.
%
%Mike Thiem, Oct 2 2017
%mike.j.thiem@gmail.com


%Read the specified number of lines from the end of the CSV file
s = tail(filename,numLines);
if s(end) == newline
    s(end) = [];
end

%Convert the string to a numeric matrix
x = parsecsv(s);

if all(isnan(x(end,:)))
    %The last line was missing some columns, so omit it
    x(end,:) = [];
end